#!/usr/bin/env python

import setuptools

PROJECT = "helloworld"
VERSION = "1.2.0"

with open("requirements.txt") as f:
    requirements = f.read().splitlines()

setuptools.setup(
    version='1.2.0',
    setup_requires=['pbr>=2.0.0'],
    pbr=True)
