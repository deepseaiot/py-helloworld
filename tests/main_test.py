import unittest
import pytest
import logging
import sys

#from src.helloworld.main import HelloWorld
from helloworld.main import main
import sys
from contextlib import contextmanager, redirect_stdout
from io import StringIO

@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err

class HelloWorldTestCase(unittest.TestCase):
    def test_output(self):
        f = StringIO()
        with redirect_stdout(f):
            main()
        out_s = f.getvalue()
        self.assertEqual(out_s, 'Hello World!\n')
