=======
helloworld
=======

Hello World python template application


## Examples

```
$ helloworld
Hello World!
```


## Installation

### From source

```bash
git clone git+https://bitbucket.org/deepseaai/py-helloworld.git
cd helloworld
pip3 install -r requirements.txt
python3 setup.py install
```

## Development
Automated testing and syntax/compliance checking using tox

```bash
sudo apt-get install tox
cd helloworld
tox
```

## Create documentation for helloworld app
```bash
sudo apt-get install tox
cd helloworld
tox -e docs
```

## License

`helloworld` is licensed under the GNU GPLv3.

## Developing

### Install

```bash
make install
```

### Run tests

```bash
make test
```

### Format code

```bash
make fmt
```
