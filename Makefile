install:
	python3 setup.py install >> /dev/null

test: install
	pytest -sv

lint: install
	flake8 dsctl --count --show-source --statistics --show-source
	flake8 dsctl --count --exit-zero --max-complexity=10 --max-line-length=127 --statistics --show-source
	# mypy dsctl/

fmt:
	black --exclude=dsctl/\(interactive\|override\).py --verbose dsctl/
